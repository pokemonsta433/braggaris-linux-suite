set shiftwidth=4 tabstop=4 softtabstop=4 autoindent smartindent
 
setlocal path=.,**
setlocal wildignore=*.o
 
compiler gcc

" doesn't work, not sure why yet. Just stick with 'make' for now
" nnoremap <buffer> <space> :silent make <bar> redraw!<CR>
 
" remap ,s and ,h to switch between header and source files
nmap ,s :find %:t:r.c<CR>
nmap ,S :sf %:t:r.c<CR>
nmap ,h :find %:t:r.h<CR>
nmap ,H :sf %:t:r.h<CR>
 
"show function name
function! ShowFuncName()
    let lnum = line(".")
    let col = col(".")
    echohl ModeMsg
    echo getline(search("^[^ \t#/]\\{2}.*[^:]\s*$", 'bW'))
    echohl None
    call search("\\%" . lnum . "l" . "\\%" . col . "c")
endfun
nmap <leader>f :call ShowFuncName()<CR> 
 
 
function! GetFuncName()
    let lnum = line(".")
    let col = col(".")
    let res = getline(search("^[^ \t#/]\\{2}.*[^:]\s*$", 'bW'))
    call search("\\%" . lnum . "l" . "\\%" . col . "c")
    return res
endfun
 
 
 
set statusline=
set statusline+=%f
set statusline+=%m
set statusline+=%=
set statusline+=%#ModeMsg#
set statusline+=\ %{GetFuncName()}
set statusline+=%#TabLineFill#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 
 